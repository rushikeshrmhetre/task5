trigger CreateContact on Account (after insert,after update,before delete) 
{
    if(Trigger.isAfter && Trigger.isInsert)
    {
        AccConTriggerHandler.CreateContact(Trigger.new);
        System.debug('>>Inside insert>>>');
        AccCreateOrUpdateForIndustryAgri.createOppIfAccTypeAgri(Trigger.new);       
    }
       
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        AccCreateOrUpdateForIndustryAgri.updateAcc( Trigger.newMap,Trigger.oldMap);
        AccCreateOrUpdateForIndustryAgri.updateOwner( Trigger.oldMap,Trigger.newMap);
    }
    if(Trigger.isBefore && Trigger.isDelete)
    {
        AccConDeleDepen.deleteAccountDepen(Trigger.old);
    }
}