trigger OpportunityTrigger on Opportunity (before delete,after update)
{
    if(Trigger.isAfter && Trigger.isdelete)
    {
        OpportunityHandler.deleteOpp(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isupdate)
    {
        OpportunityHandler.SendMailIfOppClosedWon(Trigger.newMap,Trigger.oldMap);
    }
}