public class AccConDeleDepen 
{
    public static void deleteAccountDepen(List <Account> lstAcc)
    {
        List<Contact> lstcon = [SELECT AccountId FROM Contact];
        for(Account objAcc : lstAcc)
        {
            for(Contact objCon : lstcon)
            {
                if(objAcc.Id == objCon.AccountId)
                    
                {
                    ObjAcc.addError('Account with associated Contact can not be deleted.');
                }
            }
        }
    }
    
}