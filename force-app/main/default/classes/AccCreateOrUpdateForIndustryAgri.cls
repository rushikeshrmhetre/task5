public class AccCreateOrUpdateForIndustryAgri
{
    public static boolean pass = false;
    public static void createOppIfAccTypeAgri(List <Account> lstAccNew)
    {
        List<Opportunity> lstOpp = new List<Opportunity>();
        for(Account varAcc: lstAccNew)
        {
            If(varAcc.Industry == 'Agriculture')
            {
                Opportunity op =new Opportunity();
                op.Name= varAcc.Name;
                op.AccountId=varAcc.Id;
                op.StageName='Prospecting';
                op.Amount=0;
                op.CloseDate=System.today()+90;
                
                lstOpp.add(op);
            }          
        }
        
        if(!lstOpp.isEmpty())
            insert lstOpp;    
    }
    
    public static void updateAcc(Map<Id,Account> newmap, Map<Id,Account> oldmap)
    {
        
        List<Opportunity> lstOpps = new List<Opportunity>();
        for(Id accId : newmap.KeySet())
        {
            Account newAcc = newmap.get(accId);
            Account oldAcc = oldmap.get(accId);
            if(newAcc.Id == oldAcc.Id && newAcc.Industry == 'Agriculture' && newAcc.Industry != oldAcc.Industry)
            {
                lstOpps.add(new Opportunity(Name = newAcc.Name, AccountId = newAcc.Id, StageName = 'Prospecting', Amount = 0, CloseDate = System.today() + 90));
            }
        }
        
        if(!pass && !lstOpps.isEmpty())
        {
            insert lstOpps;
            pass = true;
        }
    }
    
                   
    public static void updateOwner(Map<Id, Account> mapAcc_old, Map<Id, Account> mapAcc_new)
    {
        Map<Id,Id> mapAccIDOwnerID= new Map<Id,Id>();
        List<Contact> lstCon=[SELECT OwnerId, AccountId FROM Contact];  
        List<Contact> lstConUpdate= new List<Contact> ();
        for(Account objAcc_old : mapAcc_old.values())
        {
            for(Account objAcc_new :mapAcc_new.values())
            {
                if((objAcc_old.Id == objAcc_new.Id) && (objAcc_old.OwnerId != objAcc_new.OwnerId))
                {
                    mapAccIDOwnerID.put(objAcc_new.Id, ObjAcc_new.OwnerId);                  
                }               
            }           
        }
        for(Contact objCon : lstCon)   
        {
            if(mapAccIDOwnerID.containsKey(objCon.AccountId))  
            {
                objCon.OwnerId = mapAccIDOwnerID.get(objCon.AccountId);
                lstConUpdate.add(objCon);
            }
        }
	}
}