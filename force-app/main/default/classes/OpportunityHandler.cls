public class OpportunityHandler
{
    // test deploy comment   
    public Static Void deleteOpp(List<Opportunity> lstOpp)
    {
     	Profile profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];   
        for(Opportunity objOpp : lstOpp) 
        {
           
             // accessing Profile name through profileId
            
            if(profileName.Name != 'System Administrator' && objOpp.StageName=='Closed Won')
            {
                objOpp.addError('You dont have Permission to delelet this Opportunity');
            }
            
        }
    } 
    
    
    public Static void SendMailIfOppClosedWon(Map<Id,Opportunity> newmapopp , Map<Id,Opportunity> oldmapopp)
    {
        set<Id> accsetId=new set<Id>();   // set for account ids
        set<Id> usersetId=new set<Id>();   // set for user ids
        List<Opportunity> lstOpp=new List<Opportunity>();
        
        for(Id oppId: newmapopp.keyset())    // iterating on newmap record (key values i.e. Id(opportunity))
        {
            Opportunity objoldopp=oldmapopp.get(oppId);   //  Id store in  
            Opportunity objnewopp=newmapopp.get(oppId);	  // new record id
            
            if(objnewopp.StageName=='Closed Won' && objnewopp.StageName!=objoldopp.StageName)    
            {
                accsetId.add(objnewopp.AccountId);     //if old and new stagenames are different then store AccountId and ownerId
                usersetId.add(objoldopp.OwnerId);        
                lstOpp.add(objnewopp);
            }
        }
        List<Messaging.SingleEmailMessage> lstMsg= new list<Messaging.SingleEmailMessage>();
        Map<Id,Account> mapIdToAccount= new Map <Id,Account>([SELECT Id,OwnerId,Owner.Email FROM Account WHERE Id IN :accsetId]); 
        Map<Id, User> mapIdToUser = new Map<Id, User>([SELECT Id, Email FROM User WHERE Id IN :usersetId]);
       
        for(Opportunity objOpp: lstOpp)
        {
            Messaging.SingleEmailMessage email= new Messaging.SingleEmailMessage();
            if(objOpp.OwnerId== mapIdToAccount.get(objOpp.AccountId).OwnerId)
            {
                email.setToAddresses(new List<String> {mapIdToUser.get(objOpp.OwnerId).Email});
            }
            else
            {
                email.setToAddresses(new List<String> {mapIdToUser.get(objOpp.OwnerId).Email, mapIdToAccount.get(objOpp.AccountId).Owner.Email});
            }
            email.setSubject('Opportunity - Closed Won');
            email.setPlainTextBody('Opportunity is succesfully closed Won');
            lstMsg.add(email);
        }
        Messaging.sendEmail(lstMsg);
    }
}



















/*  //  map<Id,String> newMap =[Select Id, OwnerId, Name, StageName, Account.OwnerId, Owner.Email, Account.Owner.Email From Opportunity Where StageName='Closed Won'];

List<Messaging.SingleEmailMessage> Email_list=new List<Messaging.SingleEmailMessage >();
// List<Opportunity> lstopp=new List<Opportunity>();

for(Opportunity objnewopp: newlstOpp)
{
Opportunity oldopp=oldmapOpp.get(objnewopp.Id);
{
if(oldopp.StageName!='Closed Won' && objnewopp.StageName=='Closed Won')
{

System.debug('email sent');
Messaging.SingleEmailMessage message= new Messaging.SingleEmailMessage();

if(objnewopp.OwnerId == objnewopp.Account.OwnerId) 
{
message.ToAddresses=new List<String> {objnewopp.Owner.Email};
}
else
{
message.ToAddresses=new List<String> {objnewopp.Owner.Email, objnewopp.Account.Owner.Email};
}

message.subject='Opportunity closed Won';
message.plaintextbody='Opportunity is succesfully closed Won on';
Email_list.add(message);

}

}

}
if(Email_list.size()>0)
{
Messaging.sendEmail(Email_list);
}
*/