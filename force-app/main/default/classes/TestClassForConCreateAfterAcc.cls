@isTest
public class TestClassForConCreateAfterAcc 
{
    @isTest
    static void testCreateAccount()
    {   
        
        Account objAcc = new Account();
        objAcc.Name = 'TestAccountContact';
        objAcc.Rating = 'Hot';
        objAcc.Support_Tier__c = 'Gold';
        objAcc.Industry = 'Agriculture';

        
        Test.startTest();
        insert objAcc;   
        Contact objCon = [SELECT LastName FROM Contact WHERE AccountId =: objAcc.Id];
        System.assertEquals('TestAccountContact',objCon.LastName);
        Test.stopTest();
    }

    @isTest
    static void testDeleteAccount()
    {
        Account objAcc =  new Account(Name = 'testAccountRushi');
        insert objAcc;

        List<Account> lstAcc = [SELECT ID, Name from Account WHERE Name = 'testAccountRushi'];
        try 
        {
            delete lstAcc;
        }
         catch (Exception e) 
        {
            Boolean boolErrorMsg = e.getMessage().Contains('Account with associated Contact can not be deleted.')?true : false;
            System.assertEquals(boolErrorMsg, true);
        }
    }
    
  
}