public class ContactAndLeadSearch
{
  public static List<List<SObject>> searchContactsAndLeads( String para )
    {	
        List<List<SObject>> searchresult = [FIND :para IN ALL FIELDS RETURNING Contact(FirstName, LastName), Lead(FirstName, LastName)];
   		System.debug(searchresult);
        return searchresult;
    }

}