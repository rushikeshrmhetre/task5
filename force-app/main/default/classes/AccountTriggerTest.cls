@istest
public class AccountTriggerTest {
    @isTest static void testCreateNewAccountInBulk()
    {
          List<Account> lstAcc = new List<Account>();
        for(Integer i=0; i < 200; i++)
        {
            Account acct = new Account(Name='Test Account ' + i,BillingState ='CA');
            lstAcc.add(acct);
        }
        Test.startTest();
        insert lstAcc;
        Test.stopTest();
     	for(Account checkCA: lstAcc)
        {
            System.assertEquals('CA', checkCA.ShippingState);
		}
	}

}