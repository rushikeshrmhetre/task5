public class ContactSearch 
{
     public static List<Contact> searchForContacts(String lastName,String postalCode)
     {
         List<Contact> lstCon= new List<Contact>();
        for(Contact objCon : [SELECT Id,Name FROM Contact WHERE LastName=:lastName AND MailingPostalCode=:postalCode])
        {
            lstCon.add(objCon);
        }
         System.debug(lstCon);
         return lstCon;
     }
}